package com.mattlodes.streetsahead.spotifysh.ui;

import android.content.Intent;
import android.os.Bundle;

import com.mattlodes.streetsahead.spotifysh.BuildConfig;
import com.mattlodes.streetsahead.spotifysh.MainActivity;
import com.mattlodes.streetsahead.spotifysh.R;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Screen allowing the user to login to Spotify
 * TODO : Still needs a UI
 */
public class LoginActivity extends BaseActivity {

    private static final String CLIENT_ID = BuildConfig.SPOTIFY_CLIENT_ID;
    private static final String REDIRECT_URI = BuildConfig.REDIRECT_URI;
    private static final int    REQUEST_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Timber.tag("LoginActivity");

        startLogin(new String[]{"user-read-private", "streaming"});
    }

    private void startLogin(String[] scopes){
        final AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(
                                                            CLIENT_ID,
                                                            AuthenticationResponse.Type.TOKEN,
                                                            REDIRECT_URI);
        builder.setScopes(scopes);
        AuthenticationRequest request = builder.build();
        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            if (response.getType() == AuthenticationResponse.Type.TOKEN) {
                Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
                mUser = playerConfig;
                startMainActivity();
            }else{
                Timber.d("Login was unsuccessful");
                // TODO : Error handling here when authentication fails
            }
        }
    }

    private void startMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
