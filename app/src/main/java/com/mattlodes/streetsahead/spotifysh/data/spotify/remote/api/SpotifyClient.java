package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api;

import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.interceptor.AuthTokenInterceptor;
import com.mattlodes.streetsahead.spotifysh.ui.CategoryAdapter;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;



/**
 *  Handles creation of HttpClient used for retrofit.
 *  SpotifyClient creates instances of SpotifyService which are used to call the external api
 */
public class SpotifyClient {
    public static final String SPOTIFY_API_ENDPOINT = "https://api.spotify.com/v1/";

    private static OkHttpClient mHttpClient;  //OkHttpClient version 3 is immutable
    private String mAccessToken;

    /**
     * Constructor without authentication by default
     * @param oauthToken
     * @param mAdapter
     */
    public SpotifyClient(String oauthToken, CategoryAdapter mAdapter) {
        initHttpClient();
    }

    /**
     * Constructor with AccessToken
     * @param accessToken
     */
    public SpotifyClient(String accessToken){
        mAccessToken = accessToken;
        initHttpClient();
        setAuthorizationHeader(mAccessToken);
    }

    /**
     * Returns a new instance of SpotifyService to make api calls with
     * @return {@see SpotifyService}
     */
    public SpotifyService getService(){ return initService(); }

    /**
     * Sets the users OAuth token. Will need to get an updated SpotifyService object if stored
     * elsewhere
     * @param accessToken
     */
    public void setAccessToken(String accessToken){
        mAccessToken = accessToken;
        setAuthorizationHeader(mAccessToken);
    }

    private void initHttpClient(){
        if(mHttpClient == null){ mHttpClient = new OkHttpClient();}
    }

    private void initHttpClient(Interceptor interceptor){
        mHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    private void setAuthorizationHeader(String mAccessToken){
        initHttpClient(new AuthTokenInterceptor(mAccessToken));

    }

    private SpotifyService initService(){
        return new Retrofit.Builder()
                .baseUrl(SPOTIFY_API_ENDPOINT)
                .client(mHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(SpotifyService.class);
    }

    private OkHttpClient getHttpClient(){
        if(mHttpClient == null){ mHttpClient = new OkHttpClient();}
        return mHttpClient;
    }


}
