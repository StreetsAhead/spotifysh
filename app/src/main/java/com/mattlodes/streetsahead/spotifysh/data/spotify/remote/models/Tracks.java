package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

import java.util.List;

/**
 *  A List<Track>, corresponds to json response from Spotify api
 */
@Parcel
public class Tracks {
    public List<Track> tracks;

    @Override
    public String toString() {
        return "Tracks{" +
                "tracks=" + tracks +
                '}';
    }
}
