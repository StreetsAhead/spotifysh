package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api;


import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Album;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Albums;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Artist;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Categories;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.NewReleases;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Track;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Tracks;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Api network calls that are available.
 * Every call will return an Observable, so it needs a Subscriber
 * This class is created through SpotifyClient
 */
public interface SpotifyService {


    /**
     * Key whose value is an ISO 3166-1 alpha-2 country code.
     * Determines the country to limit results from
     */
    String COUNTRY = "country";

    /**
     * Key whose value is the integer number of results to be returned
     */
    String LIMIT  = "limit";

    /**
     * Key whose value is the integer index of the first result to show
     */
    String OFFSET = "offset";


    /**
     * Search Artist
     **/

    /**
     * Information for a single artist searched by ID
     * @param artistId : Spotify unique artist ID
     * @see <a href="https://developer.spotify.com/web-api/console/get-artist/">Get Artist</a>
     *      for details and sample output
     * @return {@see Artist} as a stream
     */
    @GET("artists/{id}")
    Observable<Artist> getArtist(@Path("id") String artistId);


    /**
     * Get albums from an Artist using Artist ID
     * @param artistId : Spotify unique artist ID
     * @see <a href="https://developer.spotify.com/web-api/console/get-artist-albums/">Get
     * Artist Albums</a>
     *      for details and sample output
     * @return {@see Artist} as a stream
     */
    @GET("/artists/{id}/albums")
    Observable<Albums> getArtistAlbums(@Path("id") String artistId, @QueryMap Map<String, Object> params);




    /**
     * Search Album
     **/

    /**
     * Information for a single album searched by ID
     * @param albumId : Spotify unique album ID
     * @see <a href="https://developer.spotify.com/web-api/console/get-album/">Get Album</a>
     *      for details and sample output
     * @return {@see Album} as a stream
     */
    @GET("albums/{id}")
    Observable<Album> getAlbum(@Path("id") String albumId);

    @GET("/albums/{id}/tracks")
    Observable<Tracks> getTracksFromAlbum(@Path("id") String albumId, @QueryMap Map<String, Object> params);




    /**
     * Search Song
     **/

    /**
     * Information for a single song searched by ID
     * @param trackId : Spotify unique track ID
     * @see <a href="https://developer.spotify.com/web-api/console/get-track/">Get Track</a>
     *      for details and sample output
     * @return {@see Track} as a stream
     */
    @GET("tracks/{id}")
    Observable<Track> getTrack(@Path("id") String trackId);




    /**
     * Browse
     **/

    @GET("browse/new-releases")
    Observable<NewReleases> getNewReleases();


    /**
     * Gets list of newest albums released.
     * Optional parameters: 'country:XX', an ISO 3166-1 alpha-2 country code
     *                      'limit:#', max number of items returned, Min=1, Max=50, default=20
     *                      'offset:#', index of first result, defaul=0, the first item
     * are 'country:XX'('US' for example), 'limit: int', and 'offset: int'
     *
     * @see <a href="https://developer.spotify.com/web-api/get-list-new-releases/">List New
     * Releases</a> for details
     *      for details and sample output
     * @param params : mapping of optional parameters and their values
     * @return {@see Albums} as a stream
     */
    @GET("browse/new-releases")
    Observable<NewReleases> getNewReleases(@QueryMap Map<String, Object> params);


    @GET("browse/categories")
    Observable<Categories> getCategories();

}
