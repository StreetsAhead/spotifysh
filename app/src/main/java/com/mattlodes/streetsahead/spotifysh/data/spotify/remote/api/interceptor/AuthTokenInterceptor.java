package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.interceptor;


import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.oauth.AuthorizationHeader;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 * A {@see okhttp3.Interceptor} that adds the Spotify user OAuth token to the header
 */
public class AuthTokenInterceptor implements Interceptor{
    private String mAccessToken;

    public AuthTokenInterceptor(String accessToken){
        mAccessToken = accessToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        AuthorizationHeader authHeader = new AuthorizationHeader(mAccessToken);
        request = request.newBuilder().addHeader(authHeader.getKey(), authHeader.getValue()).build();
        return chain.proceed(request);
    }




}
