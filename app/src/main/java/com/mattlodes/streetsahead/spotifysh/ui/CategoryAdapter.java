package com.mattlodes.streetsahead.spotifysh.ui;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mattlodes.streetsahead.spotifysh.R;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 *  Displays the Icon associated with the category in a grid
 */
public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Activity mCallingActivity;
    private final LayoutInflater mLayoutInflater;
    private ArrayList<Category> mData;
    private static int width;

    public CategoryAdapter(Activity callingActivity) {
        mCallingActivity = callingActivity;
        mLayoutInflater = callingActivity.getLayoutInflater();
        DisplayMetrics display = callingActivity.getResources().getDisplayMetrics();
        width = (int)(display.heightPixels/display.density);
    }

    /**
     *
     * @param data
     */
    public void setData(ArrayList<Category> data){
        mData = data;
    }

    /**
     * Append to existing data
     * @param data
     */
    public void appendData(ArrayList<Category> data){
        if(mData == null){ mData = data; }
        else{ mData.addAll(data); }
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.category, parent, false);
        CategoryHolder holder = new CategoryHolder(v);
        return holder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Picasso.with(mCallingActivity).load(mData.get(position).icons.get(0).url)
               .into(((CategoryHolder) holder).mIcon);

    }

    @Override
    public int getItemCount() {
        if(mData == null){ return 0; }
        else{ return mData.size(); }
    }



    public static class CategoryHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.icon)ImageView mIcon;

        public CategoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, width);
            mIcon.setLayoutParams(params);

        }
    }
}
