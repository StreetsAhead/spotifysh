package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

import java.util.List;

/**
 *  Album model corresponding to json response from Spotify
 *  @see <a href="https://developer.spotify.com/web-api/console/get-album/">Get Album</a>
 *      for details and sample response
 */
@Parcel
public class Album {
    public String album_type;
    public List<String> available_markets;
    public String id;
    public String name;
    public int popularity;
    public String uri;
    public String type;
    public List<Image> images;
    public Artist artist;

    @Override
    public String toString() {
        return "Album{" +
                "album_type='" + album_type + '\'' +
                ", available_markets=" + available_markets +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", popularity=" + popularity +
                ", uri='" + uri + '\'' +
                ", type='" + type + '\'' +
                ", images=" + images +
                ", artist=" + artist +
                '}';
    }
}
