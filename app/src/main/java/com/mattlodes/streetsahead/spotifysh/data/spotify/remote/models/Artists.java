package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

import java.util.List;

/**
 *  A List<Artist>, corresponds to json response from Spotify api
 */
@Parcel
public class Artists {
    public List<Artist> artists;

    @Override
    public String toString() {
        return "Artists{" +
                "artists=" + artists +
                '}';
    }
}
