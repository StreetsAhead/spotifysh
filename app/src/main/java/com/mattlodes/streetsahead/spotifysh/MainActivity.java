package com.mattlodes.streetsahead.spotifysh;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.GridLayout;

import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.SpotifyClient;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.SpotifyManager;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Album;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Artist;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Track;
import com.mattlodes.streetsahead.spotifysh.ui.BaseActivity;
import com.mattlodes.streetsahead.spotifysh.ui.CategoryAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * First View after successful authentication.
 * Displays a list of Categories to choose from
 */
public class MainActivity extends BaseActivity{

    private SpotifyClient mSpotifyClient;

    @Bind(R.id.album_grid)
    RecyclerView         mAlbumGrid;
    @Bind(R.id.fab)
    FloatingActionButton mFab;

    public static int NUM_COLUMNS = 2;
    private CategoryAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Timber.tag("MainActivity");
        setRecyclerView();
        setAdapter();

        SpotifyManager manager = new SpotifyManager(mUser.oauthToken, mAdapter);
        mSpotifyClient = manager.getClient();
        
//        manager.getCategories();
//        manager.searchNewAlbums();
    }

    private void setRecyclerView() {
        GridLayoutManager mLayoutManager = new GridLayoutManager(this, NUM_COLUMNS, GridLayout.VERTICAL, false);
        mAlbumGrid.setLayoutManager(mLayoutManager);
    }

    private void setAdapter() {
        mAdapter = new CategoryAdapter(this);
        mAlbumGrid.setAdapter(mAdapter);
    }


    // Basic sanity test
    public Subscriber<Artist> logArtistSubscriber = new Subscriber<Artist>() {
        @Override
        public void onCompleted() {}

        @Override
        public void onError(Throwable e) {}

        @Override
        public void onNext(Artist artist) {
            // TODO
            Timber.d(artist.toString());
        }
    };

    public Subscriber<Album> logAlbumSubscriber = new Subscriber<Album>() {
        @Override
        public void onCompleted() { }

        @Override
        public void onError(Throwable e) {}

        @Override
        public void onNext(Album album) {
            // TODO
            Timber.d(album.toString());
        }
    };

    public Subscriber<Track> logTrackSubscriber = new Subscriber<Track>() {
        @Override
        public void onCompleted() {}

        @Override
        public void onError(Throwable e) {}

        @Override
        public void onNext(Track track) {
            // TODO
            Timber.d(track.toString());
        }
    };

    public void getArtistAndLog() {
        mSpotifyClient.getService().getArtist("12Chz98pHFMPJEknJQMWvI")
                      .subscribeOn(Schedulers.newThread())
                      .subscribe(logArtistSubscriber);
    }

    public void getAlbumAndLog() {
        mSpotifyClient.getService().getAlbum("6yHliVO6vgzdJFAlmpVpKy")
                      .subscribeOn(Schedulers.newThread())
                      .subscribe(logAlbumSubscriber);
    }

    public void getTrackAndLog() {
        mSpotifyClient.getService().getTrack("2BsaECzKFhYT81E0uVZEv4")
                      .subscribeOn(Schedulers.newThread())
                      .subscribe(logTrackSubscriber);
    }


}
