package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.oauth;

/**
 *  Basic data object for storing oauth token
 */
public class AccessToken {
    private String mToken;

    public AccessToken(String token) {
        mToken = token;
    }

    public AuthorizationHeader getAuthHeader(){
        return new AuthorizationHeader(mToken);
    }
}
