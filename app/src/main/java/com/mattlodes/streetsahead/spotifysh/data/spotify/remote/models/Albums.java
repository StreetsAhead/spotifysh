package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

/**
 *
 */
public class Albums {
    public Paging<Album> albums;

    @Override
    public String toString() {
        return "Albums{" +
                "albums=" + albums +
                '}';
    }
}
