package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

/**
 *  List of Categories from json response
 *  @see <a href="https://developer.spotify.com/web-api/object-model/#category-object">Category</a>
 *      for details and sample response
 */
public class Categories {
    public Paging<Category> categories;

    @Override
    public String toString() {
        return "Categories{" +
                "categories=" + categories +
                '}';
    }
}
