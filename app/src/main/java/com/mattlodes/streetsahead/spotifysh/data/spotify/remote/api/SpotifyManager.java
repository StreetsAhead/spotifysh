package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api;

import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Album;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Categories;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.Category;
import com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models.NewReleases;
import com.mattlodes.streetsahead.spotifysh.ui.CategoryAdapter;

import java.util.ArrayList;

import rx.Subscriber;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Wrapper for easier access to API
 * Initialize class with access token and RecyclerView Adapter
 */
public class SpotifyManager {

    private SpotifyClient mClient;
    private ArrayList<Album> mAlbums;

    private CategoryAdapter mAdapter;

    // TODO : Change to generic RecyclerView Adapter
    public SpotifyManager(String accessToken, CategoryAdapter adapter) {
        mClient = new SpotifyClient(accessToken);
        mAdapter = adapter;
    }

    /**
     *
     * @return SpotifyClient
     */
    public SpotifyClient getClient(){ return mClient; }

    private Subscriber<NewReleases> getNewAlbumSubscriber() {
        return new Subscriber<NewReleases>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                // TODO Handle error
            }

            @Override
            public void onNext(NewReleases albums) {
                // TODO Somthing useful here
                for(Album album : albums.albums){
                    Timber.d(album.name);
                }
            }
        };
    }

    private Subscriber<Categories> getCategoriesSubscriber() {
        return new Subscriber<Categories>() {
            ArrayList<Category> categoryList = new ArrayList<>();

            @Override
            public void onCompleted() {
                Timber.d("New Album Search onCompleted");
                mAdapter.appendData(categoryList);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(Throwable e) {
                // TODO Handle error
            }

            @Override
            public void onNext(Categories categories) {
                for (Category category : categories.categories.items) {
                    Timber.d(category.name);
                    categoryList.add(category);
                }
            }
        };
    }

    public void searchNewAlbums() {
        mClient.getService().getNewReleases().subscribeOn(Schedulers.newThread())
               .subscribe(getNewAlbumSubscriber());
    }


    public void getCategories() {
        mClient.getService().getCategories().subscribeOn(Schedulers.newThread())
               .subscribe(getCategoriesSubscriber());
    }
}
