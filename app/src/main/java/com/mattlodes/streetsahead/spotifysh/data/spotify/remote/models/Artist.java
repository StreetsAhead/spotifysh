package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

import java.util.List;
import java.util.Map;

/**
 *  Artist model corresponding to json response from Spotify
 *  @see <a href="https://developer.spotify.com/web-api/console/get-artist/">Spotify Artist</a>
 *      for details and sample response
 */
@Parcel
public class Artist {
    public String href;
    public String id;
    public String name;
    public String type;
    public int popularity;
    public String uri;

    public Map<String, String> external_urls;
    public List<String>genres;

    @Override
    public String toString() {
        return "Artist{" +
                "href='" + href + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", popularity=" + popularity +
                ", uri='" + uri + '\'' +
                ", external_urls=" + external_urls +
                ", genres=" + genres +
                '}';
    }
}
