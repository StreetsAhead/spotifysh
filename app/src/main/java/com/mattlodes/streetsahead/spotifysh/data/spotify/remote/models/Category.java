package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

import java.util.List;

/**
 *  Music Category model corresponding to json response from Spotify
 *  @see <a href="https://developer.spotify.com/web-api/object-model/#category-object">Category</a>
 *      for details and sample response
 */
@Parcel
public class Category {
    public String href;
    public List<Image> icons;
    public String id;
    public String name;



    @Override
    public String toString() {
        return "Category{" +
                "href='" + href + '\'' +
                ", icons=" + icons +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
