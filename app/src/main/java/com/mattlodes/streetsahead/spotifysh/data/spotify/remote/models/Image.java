package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

/**
 *  Corresponds to json response from Spotify api.
 *  Contains url to image source, and the dimensions
 */
@Parcel
public class Image {
    public String url;
    public int width;
    public int height;

    @Override
    public String toString() {
        return "Image{" +
                "url='" + url + '\'' +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
