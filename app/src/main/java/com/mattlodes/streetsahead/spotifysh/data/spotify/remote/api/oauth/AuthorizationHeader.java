package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.api.oauth;

import com.mattlodes.streetsahead.spotifysh.util.http.Header;

/**
 *  Extension of {@see Header} that takes only an access token
 */
public class AuthorizationHeader extends Header {
    private static final String KEY = "Authorization";

    public AuthorizationHeader(String mValue) {
        super(KEY, mValue);
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public String getValue() {
        String value = super.getValue();
        if(value == null){ return "Bearer ";}
        else{ return "Bearer " + value;}
    }
}
