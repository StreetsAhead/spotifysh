package com.mattlodes.streetsahead.spotifysh;

import android.app.Application;

import timber.log.Timber;

/**
 * Initializes Timber logging util
 */
public class BaseApplication extends Application{
    @Override
    public void onCreate(){
        super.onCreate();

        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        }else{
            // TODO Initialize and replace with crash report( probably Crashlytics?)
            Timber.plant(new Timber.DebugTree());
        }
    }
}
