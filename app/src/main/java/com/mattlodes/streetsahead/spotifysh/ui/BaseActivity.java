package com.mattlodes.streetsahead.spotifysh.ui;

import android.support.v7.app.AppCompatActivity;

import com.spotify.sdk.android.player.Config;

/**
 *  Activity containing functionality common across all activities
 */
public class BaseActivity extends AppCompatActivity{
    protected static Config mUser;
}
