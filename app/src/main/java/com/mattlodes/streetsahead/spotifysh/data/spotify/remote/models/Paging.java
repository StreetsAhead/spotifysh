package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import java.util.List;

/**
 *  The base information that is included whenever pagination is used
 *  @see <a href="https://developer.spotify.com/web-api/object-model/#paging-object">Paging Object</a>
 */
public class Paging<T> {
    public String href;
    public List<T> items;
    public int limit;
    public String next;
    public int offset;
    public String previous;
    public int total;

    @Override
    public String toString() {
        return "Paging{" +
                "href='" + href + '\'' +
                ", items=" + items +
                ", limit=" + limit +
                ", next='" + next + '\'' +
                ", offset=" + offset +
                ", previous='" + previous + '\'' +
                ", total=" + total +
                '}';
    }
}
