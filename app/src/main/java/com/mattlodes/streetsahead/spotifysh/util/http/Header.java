package com.mattlodes.streetsahead.spotifysh.util.http;

/**
 *  Basic http header with a single key/value pair
 */
public class Header {
    private String mKey;
    private String mValue;

    public Header(){}

    public Header(String key, String value) {
        this.mKey = key;
        this.mValue = value;
    }

    public void setKey(String key) {this.mKey = key;}
    public void setValue(String value) {this.mValue = value;}

    public String getKey() {return mKey;}
    public String getValue() {return mValue;}

}
