package com.mattlodes.streetsahead.spotifysh.data.spotify.remote.models;

import org.parceler.Parcel;

import java.util.List;

/**
 *  List of Albums corresponding to json response from Spotify
 *  @see <a href="https://developer.spotify.com/web-api/object-model/">New Release</a>
 *      for details and sample response
 */
@Parcel
public class NewReleases {
    public List<Album> albums;

    @Override
    public String toString() {
        return "NewReleases{" +
                "albums=" + albums +
                '}';
    }
}
